<?php

/**
 * @file
 * Contains workflow_history.page.inc.
 *
 * Page callback for Workflow History entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Workflow History templates.
 *
 * Default template: workflow_history.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_workflow_history(array &$variables) {
  // Fetch WorkflowHistory Entity Object.
  $workflow_history = $variables['elements']['#workflow_history'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
