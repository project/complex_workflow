<?php

namespace Drupal\complex_workflow\Entity;

use Drupal\complex_workflow\StateInterface;
use Drupal\complex_workflow\Util\WorkflowItems;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\TypedData\Plugin\DataType\Uri;

/**
 * Defines the State entity.
 *
 * @ConfigEntityType(
 *   id = "state",
 *   label = @Translation("State"),
 *   handlers = {
 *     "list_builder" = "Drupal\complex_workflow\StateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\complex_workflow\Form\StateForm",
 *       "edit" = "Drupal\complex_workflow\Form\StateForm",
 *       "delete" = "Drupal\complex_workflow\Form\StateDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\complex_workflow\StateHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "state",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/workflow/{workflow}/states/{state}",
 *     "add-form" = "/admin/config/workflow/{workflow}/states/add",
 *     "edit-form" = "/admin/config/workflow/states/{state}/edit",
 *     "delete-form" = "/admin/config/workflow/states/{state}/delete",
 *     "collection" = "/admin/config/workflow/{workflow}/states"
 *   }
 * )
 */
class State extends ConfigEntityBase implements StateInterface {

  /**
   * State is active: True or False.
   *
   * @var boolean
   */
  protected $active;

  /**
   * Comments about the state.
   *
   * @var string
   */
  protected $comments;

  /**
   * Help image for this state.
   *
   * @var Uri
   */
  protected $help_image;

  /**
   * The State ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The State label.
   *
   * @var string
   */
  protected $label;

  /**
   * The state order on the Workflow
   *
   * @var integer
   */
  protected $weight;

  /**
   * The Workflow ID that the State belongs to.
   *
   * @var string
   */
  protected $workflow;

  /**
   * State constructor.
   *
   * @param array $values
   * @param string $entity_type
   */
  public function __construct(array $values = [], $entity_type = 'state') {
    parent::__construct($values, $entity_type);
  }

  /**
   * @inheritdoc
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * @inheritdoc
   */
  public function getActive() {
    return $this->active;
  }

  /**
   * @inheritdoc
   */
  public function getWorkflow() {
    return $this->workflow;
  }

  /**
   * @inheritdoc
   */
  public function getComments() {
    return $this->comments;
  }

  /**
   * @inheritdoc
   */
  public function getHelpImage() {
    return $this->help_image;
  }

  /**
   * @inheritdoc
   */
  public function getLowerWeight() {
    $items  = new WorkflowItems();
    $states = $items->getStates($this->workflow);

    $min = 999;
    foreach ($states as $state) {
      if ($state->getWeight() < $min) {
        $min = $state->getWeight();
      }
    }

    return $min;
  }

  /**
   * @inheritdoc
   */
  public function getBiggestWeight() {
    $items  = new WorkflowItems();
    $states = $items->getStates($this->workflow);

    $max = -999;
    foreach ($states as $state) {
      if ($state->getWeight() > $max) {
        $max = $state->getWeight();
      }
    }

    return $max;
  }

  /**
   * @inheritdoc
   */
  public function isEntryState() {
    return ($this->getSimpleMachineName() == 'entry_state');
  }

  /**
   * @inheritdoc
   */
  public function getSimpleMachineName() {
    return explode($this->workflow . '__', $this->id)[1];
  }

  /**
   * @inheritdoc
   */
  public function getTransitions() {

    $transitions = [];
    $workflow    = Workflow::load($this->getWorkflow());

    foreach ($workflow->getTransitions() as $transition) {
      if ($transition->getTo() == $this->id() || $transition->getFrom() == $this->id()) {
        $transitions[$transition->id()] = $transition;
      }
    }

    return $transitions;
  }

  /**
   * @inheritdoc
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {

    $workflow           = \Drupal::request()->get('workflow');
    $values['workflow'] = $workflow;

    parent::preCreate($storage, $values);
  }

  /**
   * @inheritdoc
   */
  public function save() {

    if ($this->isNew()) {
      $this->id     = $this->getWorkflow() . '__' . $this->id();
      $this->weight = $this->getBiggestWeight() + 1;
    }

    //Entry State must always be the first state.
    if ($this->isEntryState()) {
      $min = $this->getLowerWeight();
      if ($this->weight !== $min) {
        $this->weight = $min - 1;
      }
    }

    return parent::save();
  }

  /**
   * @inheritdoc
   */
  public function delete() {
    //Delete transitions related to the current state.
    $transitions = $this->getTransitions();
    foreach ($transitions as $transition) {
      $transition->delete();
    }

    parent::delete();
  }

}
