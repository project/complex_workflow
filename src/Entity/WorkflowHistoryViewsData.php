<?php

namespace Drupal\complex_workflow\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Workflow History entities.
 */
class WorkflowHistoryViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['workflow_history']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Workflow History'),
      'help' => $this->t('The Workflow History ID.'),
    );

    return $data;
  }

}
