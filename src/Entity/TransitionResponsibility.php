<?php

namespace Drupal\complex_workflow\Entity;

use Drupal\complex_workflow\TransitionResponsibilityInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Transition responsibility entity.
 *
 * @ConfigEntityType(
 *   id = "transition_responsibility",
 *   label = @Translation("Transition responsibility"),
 *   handlers = {
 *     "list_builder" = "Drupal\complex_workflow\TransitionResponsibilityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\complex_workflow\Form\TransitionResponsibilityForm",
 *       "edit" = "Drupal\complex_workflow\Form\TransitionResponsibilityForm",
 *       "delete" = "Drupal\complex_workflow\Form\TransitionResponsibilityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\complex_workflow\TransitionResponsibilityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "transition_responsibility",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/workflow/{workflow}/transitions/{transition}/transition_responsibility/{transition_responsibility}",
 *     "add-form" = "/admin/config/workflow/{workflow}/transitions/{transition}/transition_responsibility/add",
 *     "edit-form" = "/admin/config/workflow/{transition_responsibility}/edit",
 *     "delete-form" = "/admin/config/workflow/transition_responsibility/{transition_responsibility}/delete",
 *     "collection" = "/admin/config/workflow/{workflow}/transitions/{transition}/transition_responsibility",
 *     "matrix" = "/admin/config/workflow/{workflow}/transition_responsibility"
 *   }
 * )
 */
class TransitionResponsibility extends ConfigEntityBase implements TransitionResponsibilityInterface {

  /**
   * This is a true or false value, true indicating that all assignments for
   * this responsibility during the parent transition will need to have approved
   * in order for the transition to the next status to complete.
   *
   * @var boolean
   */
  protected $all_assigned_must_approve;

  /**
   * The approver comments
   *
   * @var string
   */
  protected $comments;

  /**
   * The Transition responsibility ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Transition responsibility label.
   *
   * @var string
   */
  protected $label;

  /**
   * This is the minimum number of approvals allowed for this responsibility to
   * progress the parent transition to the next status. Typical values are “0”
   * (zero) or “1”. If set to “0” (zero), no approvals for this responsibility
   * are necessary to transition to the next status.
   *
   * @var boolean
   */
  protected $min_allowed_to_approve;

  /**
   * User role for this responsibility.
   *
   * @var string
   */
  protected $role;

  /**
   * The Transition ID for this responsibility.
   *
   * @var string
   */
  protected $transition;

  /**
   * The responsibility approval order.
   *
   * @var integer
   */
  protected $weight;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @inheritdoc
   */
  public function isAllAssignedMustApprove() {
    return $this->all_assigned_must_approve;
  }

  /**
   * @inheritdoc
   */
  public function getComments() {
    return $this->comments;
  }

  /**
   * @inheritdoc
   */
  public function getMinAllowedToApprove() {
    return $this->min_allowed_to_approve;
  }

  /**
   * @inheritdoc
   */
  public function getRole() {
    return $this->role;
  }

  /**
   * @inheritdoc
   */
  public function getTransition() {
    return $this->transition;
  }

  /**
   * @inheritdoc
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * @inheritdoc
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    $this->entityTypeManager = \Drupal::entityTypeManager();
  }

  /**
   * @inheritdoc
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    $transition = \Drupal::request()->get('transition');
    $values['transition'] = $transition;

    parent::preCreate($storage, $values);

  }

  /**
   * @inheritdoc
   */
  public function save() {

    if ($this->isNew()) {
      $this->id = $this->get('transition') . '__' . $this->get('role');
    }

    return parent::save();
  }


  public function getWorkflow() {
    $transition = $this->entityTypeManager->getStorage('transition')->load($this->getTransition());
    return $transition->getWorkflow();
  }

}
