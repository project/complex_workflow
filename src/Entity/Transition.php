<?php

namespace Drupal\complex_workflow\Entity;

use Drupal\complex_workflow\TransitionInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\UserSession;

/**
 * Defines the Transition entity.
 *
 * @ConfigEntityType(
 *   id = "transition",
 *   label = @Translation("Transition"),
 *   handlers = {
 *     "list_builder" = "Drupal\complex_workflow\TransitionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\complex_workflow\Form\TransitionForm",
 *       "edit" = "Drupal\complex_workflow\Form\TransitionForm",
 *       "delete" = "Drupal\complex_workflow\Form\TransitionDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\complex_workflow\TransitionHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "transition",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/workflow/{workflow}/transitions/{transition}",
 *     "add-form" = "/admin/config/workflow/{workflow}/transitions/add/{from}/{to}",
 *     "edit-form" = "/admin/config/workflow/transitions/{transition}/edit",
 *     "delete-form" = "/admin/config/workflow/transitions/{transition}/delete",
 *     "collection" = "/admin/config/workflow/{workflow}/transition"
 *   }
 * )
 */
class Transition extends ConfigEntityBase implements TransitionInterface {

  /**
   * Is this Transition active?
   *
   * @var boolean
   */
  protected $active;

  /**
   * Transition comments.
   *
   * @var string
   */
  protected $comments;

  /**
   * Initial State ID for the Transition.
   *
   * @var string
   */
  protected $from;

  /**
   * The Transition ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Comments are required during this Transition?
   *
   * @var boolean
   */
  protected $is_comment_required;

  /**
   * The Transition label.
   *
   * @var string
   */
  protected $label;

  /**
   * The final State ID for this transition.
   *
   * @var string
   */
  protected $to;

  /**
   * Warning message to be displayed during this Transition.
   *
   * @var string
   */
  protected $warning_message;

  /**
   * Parend Workflow ID for this Transition.
   *
   * @var string
   */
  protected $workflow;

  /**
   * Transition constructor.
   *
   * @param array $values
   * @param string $entity_type
   */
  public function __construct(array $values = [], $entity_type = 'transition') {
    parent::__construct($values, $entity_type);
  }

  /**
   * @inheritdoc
   */
  public function isActive() {
    return $this->active;
  }

  /**
   * @inheritdoc
   */
  public function getComments() {
    return $this->comments;
  }

  /**
   * @inheritdoc
   */
  public function getFrom() {
    return $this->from;
  }

  /**
   * @inheritdoc
   */
  public function isCommentRequired() {
    return $this->is_comment_required;
  }

  /**
   * @inheritdoc
   */
  public function getResponsibilities() {

    $transition_responsibilities = [];

    $responsibilities = TransitionResponsibility::loadMultiple();
    foreach ($responsibilities as $responsibility) {
      if ($responsibility->get('transition') == $this->id()) {
        $transition_responsibilities[$responsibility->id()] = $responsibility;
      }
    }

    uasort($transition_responsibilities, function ($a, $b) {

      return $a->get('weight') - $b->get('weight');
    });

    return $transition_responsibilities;
  }

  /**
   * @inheritdoc
   */
  public function getTo() {
    return $this->to;
  }

  /**
   * @inheritdoc
   */
  public function getWarningMessage() {
    return $this->warning_message;
  }

  /**
   * @inheritdoc
   */
  public function getWorkflow() {
    return $this->workflow;
  }

  /**
   * @inheritdoc
   */
  public function isUserAllowed(UserSession $currentUser) {

    $responsibilities = $this->getResponsibilities();
    $isAllowed        = FALSE;

    foreach ($responsibilities as $responsibility) {
      if (in_array($responsibility->get('role'), $currentUser->getRoles(TRUE)) || $currentUser->hasPermission('bypass workflow transitions roles')) {
        $isAllowed = TRUE;
        break;
      }
    }

    return $isAllowed;
  }

  /**
   * @inheritdoc
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {

    $workflow           = \Drupal::request()->get('workflow');
    $values['workflow'] = $workflow;

    parent::preCreate($storage, $values);
  }

  /**
   * @inheritdoc
   */
  public function save() {

    if ($this->isNew()) {
      $workflow   = $this->getWorkflow();
      $from       = \Drupal::request()->get('from');
      $from_short = substr($from, strlen($workflow) + 2);
      $to         = \Drupal::request()->get('to');
      $to_short   = substr($to, strlen($workflow) + 2);

      $this->id   = $workflow . '__' . $from_short . '__' . $to_short;
      $this->from = $from;
      $this->to   = $to;
    }

    return parent::save();
  }

  public function delete() {
    //Delete responsibilities related to the current transition
    $responsibilities = $this->getResponsibilities();
    foreach ($responsibilities as $responsibility) {
      $responsibility->delete();
    }

    parent::delete();
  }

}
