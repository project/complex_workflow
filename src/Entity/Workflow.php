<?php

namespace Drupal\complex_workflow\Entity;

use Drupal\complex_workflow\WorkflowInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Workflow entity.
 *
 * @ConfigEntityType(
 *   id = "workflow",
 *   label = @Translation("Workflow"),
 *   handlers = {
 *     "list_builder" = "Drupal\complex_workflow\WorkflowListBuilder",
 *     "form" = {
 *       "add" = "Drupal\complex_workflow\Form\WorkflowForm",
 *       "edit" = "Drupal\complex_workflow\Form\WorkflowForm",
 *       "delete" = "Drupal\complex_workflow\Form\WorkflowDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\complex_workflow\WorkflowHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "workflow",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/workflow/complex_workflow/{workflow}",
 *     "add-form" = "/admin/config/workflow/complex_workflow/add",
 *     "edit-form" = "/admin/config/workflow/complex_workflow/{workflow}/edit",
 *     "delete-form" =
 *   "/admin/config/workflow/complex_workflow/{workflow}/delete",
 *     "collection" = "/admin/config/workflow/complex_workflow"
 *   }
 * )
 */
class Workflow extends ConfigEntityBase implements WorkflowInterface {
  /**
   * The Workflow description.
   *
   * @var string
   */
  protected $description;

  /**
   * The configuration of How to Change States on the Workflow.
   *
   * @var string
   */
  protected $how_to_change_state;

  /**
   * The Workflow ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Workflow label.
   *
   * @var string
   */
  protected $label;

  /**
   * The roles participating on the Workflow.
   *
   * @var array
   */
  protected $roles;

  /**
   * Show workflow comments variable.
   *
   * @var boolean
   */
  protected $show_comments;

  /**
   * Workflow constructor.
   *
   * @param array $values
   * @param string $entity_type
   */
  public function __construct(array $values = [], $entity_type = 'workflow') {
    parent::__construct($values, $entity_type);
  }

  /**
   * @inheritdoc
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @inheritdoc
   */
  public function getRoles() {
    return $this->roles;
  }

  /**
   * @inheritdoc
   */
  public function setRoles($roles) {

    $selected_roles = [];
    foreach ($roles as $role) {
      if ($role) {
        $selected_roles[$role] = $role;
      }
    }

    $this->roles = $selected_roles;
  }

  /**
   * @inheritdoc
   */
  public function getHowToChangeState() {
    return $this->how_to_change_state;
  }

  /**
   * @inheritdoc
   */
  public function getShowComments() {
    return $this->show_comments;
  }

  /**
   * @inheritdoc
   */
  public function getStates() {

    $states = State::loadMultiple();
    foreach ($states as $key => $state) {
      if ($state->getWorkflow() == $this->id()) {
        $states[$key] = $state;
      }
    }

    uasort($states, function($a, $b){
      return $a->getWeight() - $b->getWeight();
    });

    return $states;
  }

  /**
   * @inheritdoc
   */
  public function getTransitions() {

    $workflow_transitions = [];

    $transitions = Transition::loadMultiple();
    foreach ($transitions as $key => $transition) {
      if ($transition->getWorkflow() == $this->id()) {
        $workflow_transitions[$key] = $transition;
      }
    }

    uasort($workflow_transitions, function($a, $b){
      return $a->id() - $b->id();
    });

    return $workflow_transitions;
  }

  /**
   * @inheritdoc
   */
  public function getUserRoles() {
    $roles_list = user_role_names(TRUE);
    $wf_roles = $this->roles;
    $roles = [];
    foreach ($wf_roles as $role) {
      $roles[$role] = $roles_list[$role];
    }

    return $roles;

  }

  /**
   * @inheritdoc
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {

    parent::postSave($storage, $update);

    // Save Entry-state
    $entry_id = $this->id . '__entry_state';
    $entry    = \Drupal::entityTypeManager()
      ->getStorage('state')
      ->load($entry_id);

    if (empty($entry)) {
      $entry = new State([
        'id'       => $entry_id,
        'label'    => '(Entry State)',
        'weight'   => 0,
        'active'   => 1,
        'workflow' => $this->id,
      ], 'state');

      $entry->save();
    }

  }

  /**
   * @inheritdoc
   */
  public function delete() {
    //Delete related states
    $states = $this->getStates();
    foreach ($states as $state) {
      $state->delete();
    }

    parent::delete();
  }

}
