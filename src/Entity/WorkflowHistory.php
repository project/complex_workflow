<?php

namespace Drupal\complex_workflow\Entity;

use Drupal\complex_workflow\WorkflowHistoryInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\complex_workflow\Util\WorkflowUtil;
use Drupal\user\UserInterface;

/**
 * Defines the Workflow History entity.
 *
 * @ingroup complex_workflow
 *
 * @ContentEntityType(
 *   id = "workflow_history",
 *   label = @Translation("Workflow History"),
 *   module = "complex_workflow",
 *   handlers = {
 *     "access" = "Drupal\complex_workflow\Access\WorkflowHistoryAccessControlHandler",
 *     "list_builder" = "Drupal\complex_workflow\WorkflowHistoryListBuilder",
 *     "views_data" = "Drupal\complex_workflow\Entity\WorkflowHistoryViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\complex_workflow\Form\WorkflowHistoryForm",
 *       "add" = "Drupal\complex_workflow\Form\WorkflowHistoryForm",
 *       "edit" = "Drupal\complex_workflow\Form\WorkflowHistoryForm",
 *       "revert" = "Drupal\complex_workflow\Form\WorkflowHistoryRevertForm",
 *       "delete" = "Drupal\complex_workflow\Form\WorkflowHistoryDeleteForm",
 *     },
 *
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\complex_workflow\WorkflowHistoryHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "workflow_history",
 *   admin_permission = "administer workflow history entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/node/{node}/workflow",
 *     "edit-form" = "/node/{node}/workflow/history/{workflow_history}/edit",
 *     "revert-form" ="/node/{node}/workflow/history/{workflow_history}/revert",
 *     "delete-form" ="/node/{node}/workflow/history/{workflow_history}/delete",
 *   },
 *   field_ui_base_route = "workflow_history.settings",
 * )
 */
class WorkflowHistory extends ContentEntityBase implements WorkflowHistoryInterface {

//*     "canonical" = "/admin/config/workflow/history/workflow_history/{workflow_history}",
//*     "add-form" = "/admin/config/workflow/history/workflow_history/add",
//*     "edit-form" = "/admin/config/workflow/history/workflow_history/{workflow_history}/edit",
//*     "delete-form" = "/admin/config/workflow/history/workflow_history/{workflow_history}/delete",
//*     "collection" = "/admin/config/workflow/history/workflow_history",


  use EntityChangedTrait;

  /**
   * WorkflowHistory constructor.
   *
   * @param array $values
   * @param string $entity_type
   * @param bool $bundle
   * @param array $translations
   */
  public function __construct(array $values = [], $entity_type = 'workflow_history', $bundle = false, array $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {

    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {

    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {

    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {

    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {

    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {

    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {

    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFromId() {

    return $this->get('from_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getToId() {

    return $this->get('to_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getFromLabel() {

    return State::load($this->getFromId())->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getToLabel() {

    return State::load($this->getToId())->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getParentEntityId() {
    return $this->get('entity_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Workflow History entity.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Workflow History entity.'))
      ->setReadOnly(TRUE);

    $fields['workflow_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Workflow ID'))
      ->setDescription(t('The workflow the transition is related to.'))
      ->setSetting('target_type', 'workflow')
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setDescription(t('The Entity type this transition belongs to.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH)
      ->setReadOnly(TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The Entity ID this record is for.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['revision_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Revision ID'))
      ->setDescription(t('The current version identifier.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Field name'))
      ->setDescription(t('The name of the field the transition relates to.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setSetting('max_length', 32);

    $fields['from_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('From state'))
      ->setDescription(t('The {workflow_states}.sid the entity started as.'))
      ->setSetting('target_type', 'state')
      ->setReadOnly(TRUE);

    $fields['to_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('To state'))
      ->setDescription(t('The {workflow_states}.sid the entity transitioned to.'))
      ->setSetting('target_type', 'state')
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The user ID of author of the Workflow History entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Workflow History entity.'))
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type'   => 'language_select',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the term was last edited.'));

    $fields['comment'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Comment'))
      ->setDescription(t('The comment explaining this transition.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type'     => 'string_textarea',
        'weight'   => 25,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Get an array of transitions history of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  public static function getHistory(EntityInterface $entity) {

    $workflowUtil = new WorkflowUtil();

    $entity_type = $entity->getEntityTypeId();
    $entity_id   = $entity->id();
    $field_name  = $workflowUtil->getWorkflowField($entity)->getFieldStorageDefinition()->getName();

    $query = \Drupal::entityQuery('workflow_history')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->condition('field_name', $field_name)
      ->sort('created', 'desc');

    $entity_ids = $query->execute();

    $history = \Drupal::entityTypeManager()
      ->getStorage('workflow_history')
      ->loadMultiple($entity_ids);

    return $history;

  }

  /**
   * Return the last transition history.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool|mixed
   */
  public static function getLastTransitionHistory(EntityInterface $entity) {

    $workflowUtil = new WorkflowUtil();

    $entity_type = $entity->getEntityTypeId();
    $entity_id   = $entity->id();
    $field_name  = $workflowUtil->getWorkflowField($entity)->getFieldStorageDefinition()->getName();

    $query = \Drupal::entityQuery('workflow_history')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->condition('field_name', $field_name)
      ->sort('created', 'desc');

    $history_ids = $query->execute();

    $last_id = FALSE;
    foreach ($history_ids as $history_id) {
      $last_id = $history_id;
      break;
    }

    return $last_id;
  }
}
