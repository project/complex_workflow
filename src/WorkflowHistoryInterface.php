<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Workflow History entities.
 *
 * @ingroup complex_workflow
 */
interface WorkflowHistoryInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Workflow History creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Workflow History.
   */
  public function getCreatedTime();

  /**
   * Sets the Workflow History creation timestamp.
   *
   * @param int $timestamp
   *   The Workflow History creation timestamp.
   *
   * @return \Drupal\complex_workflow\WorkflowHistoryInterface
   *   The called Workflow History entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the "From" state id.
   *
   * @return string
   */
  public function getFromId();

  /**
   * Get the "To" state id.
   *
   * @return string
   */
  public function getToId();

  /**
   * Get the "From" state label.
   *
   * @return string
   */
  public function getFromLabel();

  /**
   * Get the "To" state label.
   *
   * @return string
   */
  public function getToLabel();

  /**
   * Get the Parent Entity ID.
   *
   * @return integer
   */
  public function getParentEntityId();
}
