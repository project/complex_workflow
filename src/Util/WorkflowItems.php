<?php

namespace Drupal\complex_workflow\Util;

/**
 * Class Items
 *
 * @package Drupal\complex_workflow\Util
 */
class WorkflowItems implements WorkflowItemsInterface {

  /**
   * Get all states for the given workflow.
   *
   * @return array of States
   */
  public function getStates($workflow_id) {

    $workflow = \Drupal::entityTypeManager()
                       ->getStorage('workflow')
                       ->load($workflow_id);
    $states   = $workflow->getStates();

    return $states;
  }

  /**
   * Get all transitions for the given workflow.
   *
   * @return array of Transitions
   */
  public function getTransitions($workflow_id) {

    $workflow    = \Drupal::entityTypeManager()
                          ->getStorage('workflow')
                          ->load($workflow_id);
    $transitions = $workflow->getTransitions();

    return $transitions;
  }

  /**
   * @inheritdoc
   */
  public function getWorkflowUserRoles($workflow_id) {

    $workflow   = \Drupal::entityTypeManager()
                         ->getStorage('workflow')
                         ->load($workflow_id);
    $user_roles = $workflow->getUserRoles();

    return $user_roles;
  }

  /**
   * @inheritdoc
   */
  public function getTransitionResponsibilities($transition_id) {

    $transition       = \Drupal::entityTypeManager()
                               ->getStorage('transition')
                               ->load($transition_id);
    $responsibilities = $transition->getResponsibilities();

    return $responsibilities;
  }

  /**
   * @inheritdoc
   */
  public function getWorkflows() {

    $workflows = \Drupal::entityTypeManager()
                        ->getStorage('workflow')
                        ->loadMultiple();

    uasort($workflows, function ($a, $b) {

      return $a->get('label') - $b->get('label');
    });

    return $workflows;

  }
}