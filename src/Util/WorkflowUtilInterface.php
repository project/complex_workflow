<?php

/**
 * @file
 * WorkflowUtilInterface.php
 */

namespace Drupal\complex_workflow\Util;

use Drupal\complex_workflow\TransitionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\UserSession;
use Drupal\complex_workflow\Plugin\Field\FieldType\WorkflowField;

/**
 * Interface WorkflowUtilInterface
 *
 * @package Drupal\complex_workflow\Util
 */
interface WorkflowUtilInterface {

  /**
   * Get the workflow field from some entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool|FieldDefinitionInterface The field definition if workflow
   *   field exists or false if don't.
   */
  public function getWorkflowField(EntityInterface $entity);

  /**
   * Get the id of that is being used on the workflow field in the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool|mixed
   */
  public function getSelectedWorkflow(EntityInterface $entity);

  /**
   * Get the workflow options from an entity to an user.
   *
   * User Allowed transitions;
   * How to change states;
   * Show comments.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \Drupal\Core\Session\UserSession $currentUser
   *
   * @return array
   */
  public function getWorkflowOptions(EntityInterface $entity, UserSession $currentUser);

  /**
   * Get the transitions allowed to the user.
   *
   * @param array $transitions
   * @param UserSession $current_user
   * @param string $from
   *
   * @return array
   */
  public function getUserTransitions($transitions, UserSession $current_user, $from);

  /**
   * Execute the transition on the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $transition_id
   * @param UserSession $currentUser
   * @param string $comments
   */
  public function executeTransition(EntityInterface $entity, $transition_id, UserSession $currentUser, $comments);

  /**
   * Reverts the workflow transition.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $history_id
   * @param \Drupal\Core\Session\UserSession $currentUser
   * @param $comment
   *
   * @return bool For success or failure.
   */
  public function revertTransition(EntityInterface $entity, $history_id, UserSession $currentUser, $comment);

  /**
   * Saves the workflow transition history.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \Drupal\Core\Session\UserSession $currentUser
   * @param TransitionInterface $transition
   * @param string $comments
   */
  public function saveWorkflowHistory(EntityInterface $entity, UserSession $currentUser, TransitionInterface $transition, $comments);
}