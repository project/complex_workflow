<?php

namespace Drupal\complex_workflow\Util;

/**
 * Interface WorkflowMatrixInterface
 *
 * @package Drupal\complex_workflow\Util
 */
interface WorkflowMatrixInterface {
  /**
   * Get the matrix for the workflow states.
   *
   * @param array $states
   * @param string $object_name
   * @param string $method_name
   * @param array $params
   *
   * @return array Render Array
   */
  public static function getMatrix(array $states, $object_name, $method_name, $params = []);
}