<?php

namespace Drupal\complex_workflow\Util;

/**
 * Interface WorkflowItemsInterface
 *
 * @package Drupal\complex_workflow\Util
 */
interface WorkflowItemsInterface {
  /**
   * Get all states for the given workflow.
   *
   * @param int $workflow_id
   *
   * @return array of States
   */
  public function getStates($workflow_id);

  /**
   * Get all transitions for the given workflow.
   *
   * @param int $workflow_id
   *
   * @return array of Transitions
   */
  public function getTransitions($workflow_id);

  /**
   * Get all user roles for the given workflow.
   *
   * @param $workflow_id
   *
   * @return array
   */
  public function getWorkflowUserRoles($workflow_id);

  /**
   * Get all responsibilities for the given transition.
   *
   * @param $transition_id
   *
   * @return array
   */
  public function getTransitionResponsibilities($transition_id);

  /**
   * Get all configured workflows.
   *
   * @return array
   */
  public function getWorkflows();
}