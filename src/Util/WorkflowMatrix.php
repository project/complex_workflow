<?php

namespace Drupal\complex_workflow\Util;

/**
 * Class WorkflowMatrix
 *
 * @package Drupal\complex_workflow\Util
 */
class WorkflowMatrix implements WorkflowMatrixInterface {

  /**
   * Get the matrix for the workflow states.
   *
   * @param array $states
   * @param string $object_name
   * @param string $method_name
   * @param array $params
   *
   * @return array Render Array
   */
  public static function getMatrix(array $states, $object_name, $method_name, $params = []) {

    $cel_width = 100 / count($states) - 1;

    $header = [
      t('From / To'),
    ];
    $rows   = [];

    if (count($states) > 0) {
      $j = 0;
      foreach ($states as $state) {
        if (!empty($state->id())) {
          $header[]    = [
            'data'  => $state->get('label'),
            'width' => $cel_width . '%',
            'id'    => $state->id(),
          ];
          $rows[$j][0] = [
            'data'  => $state->get('label'),
            'width' => $cel_width . '%',
            'id'    => $state->id(),
          ];
          $j++;
        }
      }

      foreach ($rows as $i => $row) {
        foreach ($header as $j => $head) {
          if ($i == ($j - 1)) {
            $rows[$i][$j] = [
              'data'  => ['#markup' => '<div class="text-center">-</div>'],
              'width' => $cel_width . '%',
            ];
          }
          elseif ($j > 0) {
            $cel_function_parameters['from'] = $row[0]['id'];
            $cel_function_parameters['to']   = $head['id'];
            $cel_function_parameters         = array_merge($params, $cel_function_parameters);

            $rows[$i][$j] = [
              'data'  => call_user_func([
                $object_name,
                $method_name,
              ], $cel_function_parameters),
              'width' => $cel_width . '%',
            ];
          }
        }
      }
    }

    $output = [
      '#type'       => 'table',
      '#header'     => $header,
      '#rows'       => $rows,
      '#sticky'     => TRUE,
      '#attributes' => ['class' => ['table-bordered', 'table-condensed']],
    ];

    return $output;

  }
}