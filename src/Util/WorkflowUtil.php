<?php

namespace Drupal\complex_workflow\Util;

use Drupal\complex_workflow\Entity\Transition;
use Drupal\complex_workflow\Entity\Workflow;
use Drupal\complex_workflow\Entity\WorkflowHistory;
use Drupal\complex_workflow\TransitionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\UserSession;


/**
 * Class WorkflowUtil
 *
 * Utilities function for Workflow Module
 *
 * @package Drupal\complex_workflow\Util
 */
class WorkflowUtil implements WorkflowUtilInterface {

  private $items;

  /**
   * WorkflowUtil constructor.
   */
  public function __construct() {

    $this->items = new WorkflowItems();
  }

  /**
   * @inheritdoc
   */
  public function getWorkflowField(EntityInterface $entity) {

    $data          = $entity;
    $workflowField = FALSE;

    if ($data) {

      // Check if entity has WorkflowField.
      $fields = $data->getFieldDefinitions();
      foreach ($fields as $field) {
        if ($field->getType() == 'workflow_field') {
          $workflowField = $field;
          break;
        }
      }

    }

    return $workflowField;

  }

  /**
   * @inheritdoc
   */
  public function getSelectedWorkflow(EntityInterface $entity) {

    $workflowSelected = FALSE;
    $workflowField    = $this->getWorkflowField($entity);
    if ($workflowField) {
      $fieldStorageDefinition = $workflowField->getFieldStorageDefinition();
      $workflowSelected       = $fieldStorageDefinition->getSetting('workflow_selected');
    }

    return $workflowSelected;
  }

  /**
   * @inheritdoc
   */
  public function getWorkflowOptions(EntityInterface $entity, UserSession $currentUser) {

    $selectedWorkflow      = $this->getSelectedWorkflow($entity);
    $userRoles             = $currentUser->getRoles(TRUE);
    $workflowConfiguration = Workflow::load($selectedWorkflow);
    $transitions           = $this->items->getTransitions($selectedWorkflow);
    $workflow_field        = $this->getWorkflowField($entity);
    $current_state         = $entity->get($workflow_field->get('field_name'));
    $current_state_name    = $current_state->getValue()[0]['value'];
    $from                  = $current_state_name;

    $user_transitions = $this->getUserTransitions($transitions, $currentUser, $from);

    $options = [];
    if ($selectedWorkflow) {
      $options = [
        'transitions'         => $user_transitions,
        'how_to_change_state' => $workflowConfiguration->get('how_to_change_state'),
        'show_comments'       => $workflowConfiguration->get('show_comments'),
      ];
    }

    return $options;
  }

  /**
   * @inheritdoc
   */
  public function getUserTransitions($transitions, UserSession $current_user, $from) {

    $user_roles          = $current_user->getRoles(TRUE);
    $allowed_transitions = [];
    foreach ($transitions as $transition) {
      if ($transition->get('from') == $from) {
        $transitionsResponsibilities = $this->items->getTransitionResponsibilities($transition->id());
        foreach ($transitionsResponsibilities as $transitionsResponsibility) {
          if (in_array($transitionsResponsibility->get('role'), $user_roles) || $current_user->hasPermission('bypass workflow transitions roles')) {
            $allowed_transitions[$transition->id()] = $transition;
          }
        }
      }
    }

    if (count($allowed_transitions)) {
      uasort($allowed_transitions, function ($a, $b) {

        return $a->get('weight') - $b->get('weight');
      });
    }

    return $allowed_transitions;
  }

  /**
   * @inheritdoc
   */
  public function executeTransition(EntityInterface $entity, $transition_id, UserSession $currentUser, $comments) {

    // Check permissions
    $transition = Transition::load($transition_id);
    //$user_transitions = $this->getUserTransitions([$transition], $currentUser->getRoles(TRUE), $transition->get('from'));

    if ($transition->isUserAllowed($currentUser)) {
      // User can execute this transition
      $workflowField = $this->getWorkflowField($entity);
      //$entity->set($workflowField, $transition->get('id'));
      $current_state = $entity->{$workflowField->getName()}[0]->get('value');
      if ($current_state != $transition->get('to')) {
        $entity->{$workflowField->getName()}[0]->set('value', $transition->get('to'));
        $entity->{$workflowField->getName()}[0]->set('from_id', $transition->get('from'));
        $entity->{$workflowField->getName()}[0]->set('uid', $currentUser->id());
        $entity->{$workflowField->getName()}[0]->set('timestamp', time());
        $entity->{$workflowField->getName()}[0]->set('comments', $comments);

        $entity->save();
        $this->saveWorkflowHistory($entity, $currentUser, $transition, $comments);
      }
    }
    else {
      drupal_set_message(t('User is now allowed to perform this transition.', 'error'));
    }

  }

  /**
   * @inheritdoc
   */
  public function revertTransition(EntityInterface $entity, $history_id, UserSession $currentUser, $comment) {

    $history = WorkflowHistory::load($history_id);
    $from    = $history->getToId();
    $to      = $history->getFromId();
    $transition = new Transition([], 'transition');
    $transition->set('from', $from);
    $transition->set('to', $to);

    $workflowField = $this->getWorkflowField($entity);
    $current_state = $entity->{$workflowField->getName()}[0]->get('value');
    if ($current_state->getValue() != $transition->get('to')) {

      $entity->{$workflowField->getName()}[0]->set('value', $to);
      $entity->{$workflowField->getName()}[0]->set('from_id', $from);
      $entity->{$workflowField->getName()}[0]->set('uid', $currentUser->id());
      $entity->{$workflowField->getName()}[0]->set('timestamp', time());
      $entity->{$workflowField->getName()}[0]->set('comment', $comment);

      $entity->save();
      $this->saveWorkflowHistory($entity, $currentUser, $transition, $comment);
    }
    else {
      return false;
    }

    return true;

  }

  /**
   * @inheritdoc
   */
  public function saveWorkflowHistory(EntityInterface $entity, UserSession $currentUser, TransitionInterface $transition, $comments) {

    $selectedWorkflow = $this->getSelectedWorkflow($entity);
    $history          = new WorkflowHistory([], 'workflow_history');
    $workflowField    = $this->getWorkflowField($entity);

    $history->set('uuid', \Drupal::service('uuid')->generate());
    $history->set('langcode', $currentUser->getPreferredLangcode());
    $history->set('workflow_id', $selectedWorkflow);
    $history->set('entity_type', $entity->getEntityTypeId());
    $history->set('entity_id', $entity->id());
    $history->set('revision_id', $entity->getRevisionId());
    $history->set('field_name', $workflowField->getName());
    $history->set('from_id', $transition->get('from'));
    $history->set('to_id', $transition->get('to'));
    $history->set('user_id', $currentUser->id());
    $history->set('created', time());
    $history->set('comment', $comments);

    $history->save();

  }

}
