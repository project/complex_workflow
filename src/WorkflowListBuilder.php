<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Workflow entities.
 */
class WorkflowListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {

    $header['label']       = $this->t('Workflow');
    $header['id']          = $this->t('Machine name');
    $header['roles']       = $this->t('Roles');
    $header['description'] = $this->t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['label'] = $entity->label();
    $row['id']    = $entity->id();

    $roles          = user_role_names(TRUE);
    $selected_roles = $entity->getRoles();
    $roles_labels   = [];
    if (!empty($selected_roles)) {
      foreach ($selected_roles as $key => $role) {
        if (array_key_exists($key, $roles)) {
          $roles_labels[$key] = $roles[$key];
        }
      }
    }

    asort($roles_labels);
    $row['roles'] = implode(', ', $roles_labels);

    $row['description'] = $entity->getDescription();

    return $row + parent::buildRow($entity);
  }

}
