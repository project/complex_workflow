<?php

namespace Drupal\complex_workflow;

use Drupal\complex_workflow\Util\WorkflowItems;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a listing of Transition responsibility entities.
 */
class TransitionResponsibilityListBuilder extends DraggableListBuilder {

  /**
   * Workflow Items.
   *
   * @var \Drupal\complex_workflow\Util\WorkflowItems
   */
  private $items;

  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {

    parent::__construct($entity_type, $storage);
    $this->items = new WorkflowItems();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label']      = $this->t('Responsibility');
    $header['role']       = $this->t('Role');
    $header['order']      = $this->t('Approval Order');
    $header['id']         = $this->t('Machine name');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();

    $user_roles  = user_role_names(TRUE);
    $row['role'] = [
      '#type'   => 'item',
      '#markup' => $user_roles[$entity->getRole()],
    ];
    $row['order'] = [
      '#type'   => 'item',
      '#markup' => $entity->getWeight(),
    ];
    $row['id']   = [
      '#type'   => 'item',
      '#markup' => $entity->id(),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {

    return 'transition_responsibility_list_form';
  }


  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type'   => 'item',
      '#markup' => $this->t("To add a new responsibility, click on 'Add Transition Responsibility' button. You may also drag it to the appropriate position.
      <br>The position represents the approval order in case this transition requires more than one responsibility role approval."),
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);

    foreach ($operations as $key => $value) {
      $operations[$key]['url']->setRouteParameters([
        'transition'                => $entity->id(),
        'workflow'                  => $entity->getWorkflow(),
        'transition_responsibility' => $entity->id(),
      ]);
    }

    return $operations;
  }

  /**
   * @inheritdoc
   */
  public function render() {
    $output = parent::render();

    $workflow   = \Drupal::request()->get('workflow');
    $transition = \Drupal::request()->get('transition');

    $back_button = Link::createFromRoute($this->t('Back'), 'entity.transition_responsibility.matrix', [
      'workflow'   => $workflow,
      'transition' => $transition,
    ], [
      'attributes' => [
        'class' => 'button',
      ],
    ])->toString();

    $output[]['#markup'] = $back_button;

    return $output;
  }

  /**
   * Load only responsibilities for the current transition.
   *
   * @inheritdoc
   */
  public function load() {

    $transition_id    = \Drupal::request()->get('transition');
    $responsibilities = $this->items->getTransitionResponsibilities($transition_id);

    return $responsibilities;
  }
}
