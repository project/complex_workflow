<?php

namespace Drupal\complex_workflow\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Form controller for Workflow History edit forms.
 *
 * @ingroup complex_workflow
 */
class WorkflowHistoryForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\complex_workflow\Entity\WorkflowHistory */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Workflow History.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Workflow History.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('complex_workflow.workflow.tab', ['node' => $entity->getParentEntityId()]);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    unset($actions['delete']);

    $actions['back']['#markup'] = Link::createFromRoute($this->t('Back'), 'complex_workflow.workflow.tab', [
      'node'   => $this->entity->getParententityId(),
    ], [
      'attributes' => [
        'class' => 'button',
      ],
    ])->toString();

    return $actions;
  }

}
