<?php

namespace Drupal\complex_workflow\Form;

use Drupal\complex_workflow\Util\WorkflowItemsInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TransitionResponsibilityForm.
 *
 * @package Drupal\complex_workflow\Form
 */
class TransitionResponsibilityForm extends EntityForm {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\complex_workflow\Util\WorkflowItemsInterface
   */
  private $items;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Workflow ID
   *
   * @var string
   */
  private $workflow_id;

  /**
   * @inheritdoc
   */
  public function __construct(WorkflowItemsInterface $items, EntityTypeManagerInterface $entityTypeManager, Request $request) {

    $this->items             = $items;
    $this->request           = $request;
    $this->workflow_id       = $request->get('workflow');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {

    $items             = $container->get('complex_workflow.workflow_items');
    $entityTypeManager = $container->get('entity_type.manager');
    $request           = $container->get('request_stack')->getCurrentRequest();

    return new static($items, $entityTypeManager, $request);

  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $responsibility = $this->entity;
    $form['label']  = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Label'),
      '#maxlength'     => 255,
      '#default_value' => $responsibility->label(),
      '#description'   => $this->t("Label for the Transition responsibility."),
      '#required'      => TRUE,
    ];

    $roles = $this->items->getWorkflowUserRoles($this->workflow_id);

    // Show only non-used roles for this transition.
    if ($responsibility->isNew()) {
      $transition = $this->entityTypeManager
        ->getStorage('transition')
        ->load($this->request->get('transition'));
    }
    else {
      $transition = $this->entityTypeManager
        ->getStorage('transition')
        ->load($responsibility->getTransition());
    }

    $roles_temp = $transition->getResponsibilities();
    $used_roles = [];
    foreach ($roles_temp as $item) {
      $used_roles[$item->getRole()] = $roles[$item->getRole()];
    }

    $list_roles = array_diff($roles, $used_roles);

    if (!$this->entity->isNew()) {
      // Add current role to option list if user is editing the responsibility.
      $list_roles[$responsibility->getRole()] = $roles[$responsibility->getRole()];

      $form['id'] = [
        '#type'         => 'value',
        '#value'        => $responsibility->id(),
        '#markup'       => $responsibility->id(),
        '#machine_name' => [
          'exists' => '\Drupal\complex_workflow\Entity\TransitionResponsibility::load',
        ],
      ];
    }

    if (!count($list_roles)) {
      drupal_set_message($this->t("All available workflow roles are already in use. You can't create new responsibility."), 'warning');
    }

    $form['role'] = [
      '#type'          => 'select',
      '#options'       => $list_roles,
      '#title'         => $this->t('User role'),
      '#description'   => $this->t('User role for this responsibility.'),
      '#default_value' => $responsibility->getRole(),
      '#required'      => TRUE,
      '#disabled'      => !$this->entity->isNew(),
    ];

    $form['weight'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Approval Order'),
      '#default_value' => $responsibility->getWeight(),
      '#required'      => TRUE,
      '#description'   => $this->t("This is an integer numeric value which should 
        be greater than 1 if entered. Typical values are 1, 2, 3, etc. This can 
        be defined when you would like to order the approvals. The system will 
        process the lower approval values first. As an example, if you would 
        like the reviewer responsibility approvals to all take place prior to 
        the supervisor approvals, you would make a transition responsibility 
        entry for the reviewer responsibility with a 10 (for example), and 
        another transition responsibility entry for the supervisor 
        responsibility with a 20 (for example). Since 20 is after 10, the 
        reviewer responsibility approvals will take place first."),
    ];

    $form['comments'] = [
      '#type'          => 'textarea',
      '#default_value' => $responsibility->getComments(),
      '#title'         => $this->t('Comments'),
      '#description'   => $this->t('Comments about the element.'),
    ];

    $form['all_assigned_must_approve'] = [
      '#type'          => 'checkbox',
      '#default_value' => $responsibility->isAllAssignedMustApprove(),
      '#title'         => $this->t('All assigned must approve '),
      '#description'   => $this->t('This is a true or false value, true indicating 
        that all assignments for this responsibility during the parent 
        transition will need to have approved in order for the transition to the 
        next status to complete.'),
    ];

    $form['min_allowed_to_approve'] = [
      '#type'          => 'number',
      '#default_value' => $responsibility->getMinAllowedToApprove(),
      '#title'         => $this->t('Min allowed to approve'),
      '#description'   => $this->t('This is the minimum number of approvals 
        allowed for this responsibility to progress the parent transition to the 
        next status. Typical values are “0” (zero) or “1”. If set to “0” (zero), 
        no approvals for this responsibility are necessary to transition to the 
        next status.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $transition_responsibility = $this->entity;
    $status                    = $transition_responsibility->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Transition responsibility.', [
          '%label' => $transition_responsibility->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Transition responsibility.', [
          '%label' => $transition_responsibility->label(),
        ]));
    }

    $url = Url::fromRoute('entity.transition_responsibility.collection', [
      'workflow'   => $this->request->get('workflow'),
      'transition' => $this->request->get('transition'),
    ]);
    $form_state->setRedirectUrl($url);
  }


}
