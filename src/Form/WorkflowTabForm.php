<?php

namespace Drupal\complex_workflow\Form;

use Drupal\complex_workflow\Access\WorkflowAccessCheck;
use Drupal\complex_workflow\Entity\Transition;
use Drupal\complex_workflow\Entity\WorkflowHistory;
use Drupal\complex_workflow\Util\WorkflowUtilInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WorkflowTabForm
 *
 * @package Drupal\complex_workflow\Form
 */
class WorkflowTabForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Drupal\complex_workflow\Util\WorkflowUtilInterface
   */
  private $workflowUtil;

  public function __construct(WorkflowUtilInterface $workflowUtil, EntityTypeManagerInterface $entityTypeManager) {

    $this->entityTypeManager = $entityTypeManager;
    $this->workflowUtil      = $workflowUtil;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {

    $workflowUtil      = $container->get('complex_workflow.workflow_util');
    $entityTypeManager = $container->get('entity_type.manager');

    return new static($workflowUtil, $entityTypeManager);
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {

    $form['#title'] = $this->t('Workflow');
    $entity         = $this->entityTypeManager->getStorage('node')->load($node);

    $wf_options          = $this->workflowUtil->getWorkflowOptions($entity, $this->currentUser()
      ->getAccount());
    $transitions         = $wf_options['transitions'];
    $how_to_change_state = $wf_options['how_to_change_state'];
    $show_comments       = $wf_options['show_comments'];

    $form['entity_id'] = [
      '#type'  => 'value',
      '#value' => $entity->id(),
    ];

    if (!count($transitions)) {
      $form['no_transitions_message'] = [
        '#type'   => 'markup',
        '#markup' => $this->t('Workflow has no transitions available.'),
      ];
    }

    if ($show_comments && count($transitions)) {
      $form['comments'] = [
        '#type'        => 'textarea',
        '#title'       => $this->t('Comments'),
        '#description' => $this->t('A comment to put in the workflow log.'),
        '#columns'     => 60,
        '#rows'        => 5,
      ];
    }

    $form['actions'] = ['#type' => 'actions'];

    switch ($how_to_change_state) {
      case 'action_buttons':
        $form['actions']['transitions'] = $this->getButtons($transitions);
        break;

    }

    $entity                     = $this->entityTypeManager->getStorage('node')
      ->load($node);
    $form['history']            = $this->getHistory($entity);
    $form['history']['#weight'] = 100;

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {

    return 'sphera_workflow_tab_form';
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entity  = $this->entityTypeManager->getStorage('node')
      ->load($form_state->getValue('entity_id'));
    $trigger = $form_state->getTriggeringElement();
    $op      = $trigger['#op'];

    switch ($op) {
      case 'action_buttons':
        $transition_id = $trigger['#name'];
        break;
    }

    $current_user = $this->currentUser();
    $this->workflowUtil->executeTransition($entity, $transition_id, $current_user->getAccount(), $form_state->getValue('comments'));

  }

  /**
   * @inheritdoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);
    $trigger = $form_state->getTriggeringElement();
    $op      = $trigger['#op'];

    $transition_id = FALSE;
    switch ($op) {
      case 'action_buttons':
        $transition_id = $trigger['#name'];
        break;
    }

    if ($transition_id) {
      $transition = Transition::load($transition_id);
      if ($transition->get('is_comment_required')) {
        $comments = $form_state->getValue('comments');
        if (strlen(trim($comments)) <= 1) {
          $form_state->setErrorByName('comments', $this->t('Please, fill the comments to execute this transition.'));
        }
      }
    }
  }

  /**
   * Get the transition Buttons.
   *
   * @param $transitions
   *
   * @return array
   */
  public function getButtons($transitions) {

    $elements = [];

    foreach ($transitions as $transition) {
      $elements[$transition->id()] = [
        '#type'  => 'submit',
        '#value' => $transition->get('label'),
        '#name'  => $transition->id(),
        '#op'    => 'action_buttons',
      ];
    }

    return $elements;
  }

  /**
   * Get the History data from the entity in a render array.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  public function getHistory(EntityInterface $entity) {

    $current_user = \Drupal::currentuser();

    $history = WorkflowHistory::getHistory($entity);
    $header  = [
      ['id' => 'date', 'data' => $this->t('Date')],
      ['id' => 'from', 'data' => $this->t('From')],
      ['id' => 'to', 'data' => $this->t('To')],
      ['id' => 'by', 'data' => $this->t('By')],
      ['id' => 'comment', 'data' => $this->t('Comment')],
      ['id' => 'operations', 'data' => $this->t('Operations')],
    ];

    $rows     = [];
    $lang     = $current_user->getPreferredLangcode();
    $timeZone = $current_user->getTimeZone();

    foreach ($history as $item) {
      $created    = \Drupal::service('date.formatter')
        ->format($item->getCreatedTime(), 'short', '', $timeZone, $lang);
      $from       = $item->getFromLabel();
      $to         = $item->getToLabel();
      $user       = $item->getOwner();
      $comment    = $item->get('comment')->value;
      $operations = $this->getHistoryOperations($entity->id(), $item->id());

      $rows[] = [
        $created,
        $from,
        $to,
        $user->getUsername(),
        ['data' => $comment],
        ['data' => $operations],
      ];
    }

    $output = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $rows,
      '#sticky' => TRUE,
    ];

    return $output;

  }

  /**
   * Get the list of operations for the workflow history.
   *
   * @return array
   */
  public function getHistoryOperations($node, $history_id) {

    $check      = new WorkflowAccessCheck();
    $can_edit   = $check->editComment($node, $history_id);
    $can_revert = $check->revertTransition($node, $history_id);
    $links      = [];

    if ($can_edit == AccessResult::allowed()) {
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url'   => Url::fromRoute('entity.workflow_history.edit_form',
          [
            'node'             => $node,
            'workflow_history' => $history_id,
          ]),
      ];
    }

    if ($can_revert == AccessResult::allowed()) {
      $links['revert'] = [
        'title' => $this->t('Revert'),
        'url'   => Url::fromRoute('entity.workflow_history.revert_form',
          [
            'node'             => $node,
            'workflow_history' => $history_id,
          ]),
      ];
    }

    if (count($links)) {
      $operations = [
        '#type'  => 'dropbutton',
        '#links' => $links,
      ];
    }
    else {
      $operations = [
        '#type'   => 'markup',
        '#markup' => $this->t('None'),
      ];
    }

    return $operations;
  }

  //private function isLastTransition() { }
}