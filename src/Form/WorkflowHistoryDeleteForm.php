<?php

namespace Drupal\complex_workflow\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Workflow History entities.
 *
 * @ingroup complex_workflow
 */
class WorkflowHistoryDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $url = parent::getCancelUrl();
    $url->setRouteParameter('node', $this->entity->getParentEntityId());

    return $url;
  }

}
