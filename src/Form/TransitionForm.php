<?php

namespace Drupal\complex_workflow\Form;

use Drupal\complex_workflow\Entity\State;
use Drupal\complex_workflow\TransitionListBuilder;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class TransitionForm.
 *
 * @package Drupal\complex_workflow\Form
 */
class TransitionForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form       = parent::form($form, $form_state);
    $transition = $this->entity;

    if ($transition->isNew()) {
      $from_id = \Drupal::request()->get('from');
      $to_id   = \Drupal::request()->get('to');
    }
    else {
      $from_id = $transition->getFrom();
      $to_id   = $transition->getTo();
    }

    $from = state::load($from_id);
    $to   = state::load($to_id);

    $form['info'] = [
      '#type'   => 'item',
      '#title' => $this->t('Transition'),
      '#markup' => $this->t('From %from to %to', ['%from' => $from->label(), '%to' => $to->label()]),
    ];

    $form['label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Label'),
      '#maxlength'     => 255,
      '#default_value' => $transition->label(),
      '#description'   => $this->t("Label for the Transition."),
      '#required'      => TRUE,
    ];

    if (!$transition->isNew()) {
      $form['id'] = [
        '#type'         => 'value',
        '#title'        => $this->t('Machine Name:'),
        '#value'        => $transition->id(),
        '#markup'       => $transition->id(),
        '#machine_name' => [
          'exists' => '\Drupal\complex_workflow\Entity\Transition::load',
        ],
      ];
    }

    $form['active'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Active'),
      '#default_value' => $transition->isNew() ? 1 : $transition->isActive(),
    ];

    $form['comments'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Comments'),
      '#default_value' => $transition->getComments(),
    ];

    $form['warning_message'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Warning Message'),
      '#default_value' => $transition->getWarningMessage(),
    ];

    $form['is_comment_required'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Is comments required'),
      '#description'   => $this->t('Will user need to fill the comments in order to perform the transition?'),
      '#default_value' => $transition->isNew() ? 0 : $transition->isCommentRequired(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $transition = $this->entity;
    $status     = $transition->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Transition.', [
          '%label' => $transition->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Transition.', [
          '%label' => $transition->label(),
        ]));
    }
    $url = Url::fromRoute('entity.transition.collection', ['workflow' => $transition->getWorkflow()]);
    $form_state->setRedirectUrl($url);
  }



}
