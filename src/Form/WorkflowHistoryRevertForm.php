<?php

namespace Drupal\complex_workflow\Form;

use Drupal\complex_workflow\Entity\WorkflowHistory;
use Drupal\complex_workflow\Util\WorkflowUtilInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WorkflowHistoryRevertForm
 *
 * @package Drupal\complex_workflow\Form
 */
class WorkflowHistoryRevertForm extends ContentEntityForm {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\complex_workflow\Util\WorkflowUtilInterface
   */
  protected $workflowUtil;

  /**
   * WorkflowHistoryRevertForm constructor.
   *
   * @param \Drupal\complex_workflow\Util\WorkflowUtilInterface $workflowUtil
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param EntityManager $entityManager
   */
  public function __construct(WorkflowUtilInterface $workflowUtil, EntityTypeManagerInterface $entityTypeManager, EntityManager $entityManager) {

    $this->entityTypeManager = $entityTypeManager;
    $this->workflowUtil      = $workflowUtil;

    parent::__construct($entityManager);
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {

    $workflowUtil      = $container->get('complex_workflow.workflow_util');
    $entityTypeManager = $container->get('entity_type.manager');
    $entityManager     = $container->get('entity.manager');
    return new static($workflowUtil, $entityTypeManager, $entityManager);
  }

  //  /**
  //   * @inheritdoc
  //   */
  //  public function getFormId() {
  //
  //    return 'complex_workflow.workflow_history.revert_form';
  //  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL, $workflow_history = NULL) {

    $entity = $workflow_history;
    $from   = $entity->getFromLabel();
    $to     = $entity->getToLabel();
    $form   = parent::buildForm($form, $form_state);

    $form['#title'] = $this->t('Revert transition from %from to %to', [
      '%from' => $from,
      '%to'   => $to,
    ]);

    $form['history_id'] = [
      '#type'  => 'value',
      '#value' => $entity->id(),
    ];

    $form['node'] = [
      '#type'  => 'value',
      '#value' => $node,
    ];

    $form['from'] = [
      '#type'  => 'value',
      '#value' => $entity->getToId(),
    ];

    $form['to'] = [
      '#type'  => 'value',
      '#value' => $entity->getFromId(),
    ];

    $form['comment'] = [
      '#type'        => 'textarea',
      '#title'       => $this->t('Comments'),
      '#description' => $this->t('A comment to put in the workflow log.'),
      '#required'    => TRUE,
      '#columns'     => 60,
      '#rows'        => 5,
    ];
//
//    $form['actions']['revert'] = [
//      '#type'  => 'submit',
//      '#value' => $this->t('Revert'),
//    ];
//
//    $form['actions']['cancel'] = [
//      '#type'       => 'link',
//      '#title'      => $this->t('Cancel'),
//      '#attributes' => ['class' => ['button']],
//      '#url'        => Url::fromRoute('complex_workflow.workflow.tab', ['node' => $node]),
//    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $history_id   = $form_state->getValue('history_id');
    $comment      = $form_state->getValue('comment');
    $history      = WorkflowHistory::load($history_id);
    $node_id      = $form_state->getValue('node');
    $node         = $this->entityTypeManager->getStorage('node')
      ->load($node_id);
    $current_user = $this->currentUser();

    if ($this->workflowUtil->revertTransition($node, $history_id, $current_user->getAccount(), $comment)) {
      drupal_set_message($this->t('Transition has been reverted.'));
    }
    else {
      drupal_set_message($this->t('Element is already on %state state', ['%state' => $history->getFromLabel()]), 'warning');
    }

    $form_state->setRedirect('complex_workflow.workflow.tab', ['node' => $node_id]);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    unset($actions['delete']);

    $actions['back']['#markup'] = Link::createFromRoute($this->t('Back'), 'complex_workflow.workflow.tab', [
      'node'   => $this->entity->getParententityId(),
    ], [
      'attributes' => [
        'class' => 'button',
      ],
    ])->toString();

    $actions['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Revert'),
    ];

    return $actions;
  }
}