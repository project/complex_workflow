<?php

namespace Drupal\complex_workflow\Form;

use Drupal\complex_workflow\Entity\State;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Class StateForm.
 *
 * @package Drupal\complex_workflow\Form
 */
class StateForm extends EntityForm {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form           = parent::form($form, $form_state);
    $workflow_state = $this->entity;

    $form['label']  = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Label'),
      '#maxlength'     => 255,
      '#default_value' => $workflow_state->label(),
      '#description'   => $this->t("Label for the Workflow State."),
      '#required'      => TRUE,
    ];

    $form['id'] = [
      '#type'          => 'machine_name',
      '#default_value' => $workflow_state->id(),
      '#machine_name'  => [
        'exists' => '\Drupal\complex_workflow\Entity\State::load',
      ],
      '#disabled'      => !$workflow_state->isNew(),
    ];

    $form['active'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Active'),
      '#default_value' => $workflow_state->isNew() ? 1 : $workflow_state->getActive(),
    ];

    $form['comments'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Workflow description'),
      '#default_value' => $workflow_state->getComments(),
    ];

    $form['help_image'] = [
      '#type'          => 'managed_file',
      '#title'         => $this->t('Help Image'),
      '#description'   => $this->t('A image to appear at workflow tab when the node has this state.'),
      '#upload_location' => 'public://complex_workflow/help_image',
      '#default_value' => [$workflow_state->getHelpImage()],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /**
     * @var State $state
     */
    $state  = $this->entity;

    $fid = $form_state->getValue('help_image')[0];
    $file = File::load($fid);
    $file->setPermanent();
    $file->save();

    $state->set('help_image', $fid);
    $status = $state->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label State.', [
          '%label' => $state->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label State.', [
          '%label' => $state->label(),
        ]));
    }

    $url = Url::fromRoute('entity.state.collection', ['workflow' => $state->getWorkflow()]);
    $form_state->setRedirectUrl($url);
  }

  /**
   * @inheritdoc
   */
  public function getOperation() {
    $operation = parent::getOperation();

    return $operation;
  }

}
