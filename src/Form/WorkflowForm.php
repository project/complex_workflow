<?php

namespace Drupal\complex_workflow\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WorkflowForm.
 *
 * @package Drupal\complex_workflow\Form
 */
class WorkflowForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $workflow      = $this->entity;
    $form['label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Label'),
      '#maxlength'     => 255,
      '#default_value' => $workflow->label(),
      '#description'   => $this->t("Label for the Workflow."),
      '#required'      => TRUE,
    ];

    $form['id'] = [
      '#type'          => 'machine_name',
      '#default_value' => $workflow->id(),
      '#machine_name'  => [
        'exists' => '\Drupal\complex_workflow\Entity\Workflow::load',
      ],
      '#disabled'      => !$workflow->isNew(),
    ];

    $form['description'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Workflow description'),
      '#default_value' => $workflow->getDescription(),
    ];

    $form['fieldset_roles'] = [
      '#type'  => 'details',
      '#title' => $this->t('Choose the roles for this Workflow'),
      '#open'  => TRUE,

    ];

    $roles_list = user_role_names(TRUE);
    asort($roles_list);
    $form['fieldset_roles']['roles'] = [
      '#type'          => 'checkboxes',
      '#options'       => $roles_list,
      '#default_value' => count($workflow->getRoles()) ? $workflow->getRoles() : [],
    ];

    $form['fieldset_options'] = [
      '#type'  => 'details',
      '#title' => $this->t('Options'),
      '#open'  => TRUE,
    ];

    $form['fieldset_options']['how_to_change_state'] = [
      '#type'          => 'select',
      '#title'         => $this->t('How to show the available states?'),
      '#default_value' => $workflow->getHowToChangeState(),
      '#options'       => [
        'action_buttons' => $this->t('Action Buttons'),
        'select_list'    => $this->t('Select List'),
        'radio_buttons'  => $this->t('Radio Buttons'),
      ],
    ];

    $form['fieldset_comments'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Comment'),
      '#markup' => $this->t("A Comment form can be shown on the Workflow 
        Transition form so that the person making a state change can record 
        reasons for doing so. The comment is then included in the content's 
        workflow history."),
      '#open'   => TRUE,
    ];

    if ($workflow->isNew()) {
      $show_comments = 1;
    }
    else {
      $show_comments = $workflow->getShowComments() === TRUE ? 1 : 0;
    }

    $form['fieldset_comments']['show_comments'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Show comment on the Workflow history tab of content'),
      '#default_value' => $show_comments,
      '#options'       => [
        '1' => $this->t('Optional'),
        '0' => $this->t('Hidden'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $workflow = $this->entity;
    // Only save the checked roles
    $workflow->setRoles($workflow->get('roles'));
    $status = $workflow->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Workflow.', [
          '%label' => $workflow->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Workflow.', [
          '%label' => $workflow->label(),
        ]));
    }
    $form_state->setRedirectUrl($workflow->urlInfo('collection'));
  }

}
