<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Transition responsibility entities.
 */
interface TransitionResponsibilityInterface extends ConfigEntityInterface {
  /**
   * This is a true or false value, true indicating that all assignments for
   * this responsibility during the parent transition will need to have approved
   * in order for the transition to the next status to complete.
   *
   * @return boolean
   */
  public function isAllAssignedMustApprove();

  /**
   * The approver comments
   *
   * @return string
   */
  public function getComments();

  /**
   * This is the minimum number of approvals allowed for this responsibility to
   * progress the parent transition to the next status. Typical values are “0”
   * (zero) or “1”. If set to “0” (zero), no approvals for this responsibility
   * are necessary to transition to the next status.
   *
   * @return boolean
   */
  public function getMinAllowedToApprove();

  /**
   * User role for this responsibility.
   *
   * @return string
   */
  public function getRole();

  /**
   * The Transition ID for this responsibility.
   *
   * @return string
   */
  public function getTransition();

  /**
   * The responsibility approval order.
   *
   * @return integer
   */
  public function getWeight();

  /**
   * Return the workflow ID that this entity belongs to.
   *
   * @return string
   */
  public function getWorkflow();
}
