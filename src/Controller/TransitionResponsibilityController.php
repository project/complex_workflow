<?php

namespace Drupal\complex_workflow\Controller;

use Drupal\complex_workflow\Util\WorkflowItemsInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TransitionResponsibilityController
 *
 * @package Drupal\complex_workflow\Controller
 */
class TransitionResponsibilityController extends ControllerBase {

  private $items;
  private $matrix;
  private $states;
  private $transitions;
  private $workflow_id;

  public function __construct(WorkflowItemsInterface $items, \Drupal\complex_workflow\Util\WorkflowMatrixInterface $matrix) {
    $this->workflow_id = \Drupal::request()->get('workflow');
    $this->items = $items;
    $this->states = $items->getStates($this->workflow_id);
    $this->transitions = $items->getTransitions($this->workflow_id);
    $this->matrix = $matrix;
  }

  /**
   * Render array for the matrix cell
   *
   * @param array $params
   *
   * @return mixed
   */
  public function cell($params = []) {

    $from = $params['from'];
    $to   = $params['to'];

    $output['#markup'] = '';

    $transitions = $this->transitions;

    if (count($transitions)) {
      foreach ($transitions as $transition) {

        if ($from == $transition->get('from') && $to == $transition->get('to')) {

          //Display current responsibilities
          $responsibilities = $this->items->getTransitionResponsibilities($transition->id());
          foreach ($responsibilities as $responsibility) {
            $resp_link = Link::createFromRoute($responsibility->get('label'), 'entity.transition_responsibility.edit_form', [
              'workflow'            => $transition->getWorkflow(),
              'transition' => $transition->id(),
              'transition_responsibility' => $responsibility->id(),
            ], [
              'attributes' => [
                'class' => 'btn btn-info',
                'style' => 'margin-left: 0px; margin-right: 0px; width: 100%',
              ],
            ])->toString();

            $output[]['#markup'] = $resp_link;
          }

          $link = Link::createFromRoute($this->t('Edit Responsibilities'), 'entity.transition_responsibility.collection', [
            'workflow'            => $transition->getWorkflow(),
            'transition' => $transition->id(),
          ], [
            'attributes' => [
              'class' => 'btn btn-primary',
              'style' => 'margin-left: 0px; margin-right: 0px; width: 100%',
            ],
          ])->toString();

          $output[]['#markup'] = $link;

        }
      }
    }

    return $output;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {

    $items = $container->get('complex_workflow.workflow_items');
    $matrix = $container->get('complex_workflow.matrix');

    return new static($items, $matrix);
  }

  public function render() {

    $output['#title'] = $this->t('Transitions Responsibilities');
    $output['matrix'] = $this->matrix->getMatrix($this->states, $this, 'cell');

    $output['#attached']['library'][] = 'complex_workflow/workflow_matrix';

    return $output;

  }

}