<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Workflow History entity.
 *
 * @see \Drupal\complex_workflow\Entity\WorkflowHistory.
 */
class WorkflowHistoryAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\complex_workflow\WorkflowHistoryInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished workflow history entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published workflow history entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit workflow history entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete workflow history entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add workflow history entities');
  }

}
