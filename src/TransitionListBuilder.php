<?php

namespace Drupal\complex_workflow;

use Drupal\complex_workflow\Util\WorkflowMatrix;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;

/**
 * Provides a listing of Transition entities.
 */
class TransitionListBuilder extends ConfigEntityListBuilder {

  private $states;

  private $transitions;

  private $workflow_id;

  /**
   * @inheritdoc
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {

    parent::__construct($entity_type, $storage);

    $this->workflow_id = \Drupal::request()->get('workflow');
    $this->states      = $this->getStates();
    $this->transitions = $this->getTransitions();
  }



  /**
   * @inheritdoc
   */
  public function render() {

    $output['#title'] = $this->t('Workflow Transitions');
    $output['matrix'] = WorkflowMatrix::getMatrix($this->states, $this, 'cell');

    $output['#attached']['library'][] = 'complex_workflow/workflow_matrix';

    return $output + parent::render();
  }

  public function cell($params = []) {

    $from = $params['from'];
    $to   = $params['to'];

    $disabled_attributes = [
      'attributes' => [
        'title' => t('Add Transition'),
        'class' => 'btn btn-default',
        'style' => 'margin-left: 0px; margin-right: 0px; width: 100%',
      ],
    ];

    $disabled_button = Link::createFromRoute(t('Disabled'),
      'entity.transition.add_form', [
        'workflow' => $this->workflow_id,
        'from'     => $from,
        'to'       => $to,
      ], $disabled_attributes)->toString();

    $output['#markup'] = $disabled_button;

    $transitions = $this->transitions;

    if (count($transitions)) {
      foreach ($transitions as $transition) {

        if ($from == $transition->getFrom() && $to == $transition->getTo()) {

          $link = Link::createFromRoute($transition->get('label'), 'entity.transition.edit_form', [
            'workflow'   => $transition->getWorkflow(),
            'transition' => $transition->id(),
          ], [
            'attributes' => [
              'class' => 'btn btn-info',
              'style' => 'margin-left: 0px; margin-right: 0px; width: 100%',
            ],
          ])->toString();

          $output['#markup'] = $link;

        }
      }
    }

    return $output;
  }

  /**
   * Get all states for the current workflow.
   *
   * @return array of States
   */
  private function getStates() {

    $workflow = \Drupal::entityTypeManager()
      ->getStorage('workflow')
      ->load($this->workflow_id);
    $states   = $workflow->getStates();

    return $states;
  }

  /**
   * Get all transitions for the current workflow.
   *
   * @return array of Transitions
   */
  private function getTransitions() {

    $workflow    = \Drupal::entityTypeManager()
      ->getStorage('workflow')
      ->load($this->workflow_id);
    $transitions = $workflow->getTransitions();

    return $transitions;
  }


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label']    = $this->t('Transition');
    $header['id']       = $this->t('Machine name');
    $header['comments'] = $this->t('Comments');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id']    = $entity->id();

    $row['comments'] = $entity->getComments();

    return $row + parent::buildRow($entity);
  }

  /**
   * @inheritdoc
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);

    foreach ($operations as $key => $value) {
      $operations[$key]['url']->setRouteParameters([
        'transition'    => $entity->id(),
        'workflow' => $entity->getWorkflow(),
      ]);
    }

    return $operations;
  }
}
