<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Workflow History entities.
 *
 * @ingroup complex_workflow
 */
class WorkflowHistoryListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Workflow History ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\complex_workflow\Entity\WorkflowHistory */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.workflow_history.edit_form', array(
          'workflow_history' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
