<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\TypedData\Plugin\DataType\Uri;

/**
 * Provides an interface for defining State entities.
 */
interface StateInterface extends ConfigEntityInterface {

  /**
   * The state order on the Workflow
   *
   * @return integer
   */
  public function getWeight();

  /**
   * State is active: True or False.
   *
   * @return boolean
   */
  public function getActive();

  /**
   * The Workflow ID that the State belongs to.
   *
   * @return string
   */
  public function getWorkflow();

  /**
   * Comments about the state.
   *
   * @return string
   */
  public function getComments();

  /**
   * Help image for this state.
   *
   * @return Uri
   */
  public function getHelpImage();

  /**
   * Get the lowest weight for the states in the current workflow.
   *
   * @return int
   */
  public function getLowerWeight();

  /**
   * Get the biggest weight for the states in the current workflow.
   *
   * @return int
   */
  public function getBiggestWeight();

  /**
   * Check if the state is Entry State.
   *
   * @return bool
   */
  public function isEntryState();

  /**
   * Return the state machine name without the workflow name on it.
   *
   * @return string
   */
  public function getSimpleMachineName();

  /**
   * Return all Transitions related to the current State.
   *
   * @return array of Transitions
   */
  public function getTransitions();
}
