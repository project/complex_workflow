<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Workflow entities.
 */
interface WorkflowInterface extends ConfigEntityInterface {

  /**
   * Get workflow description.
   *
   * @return string
   */
  public function getDescription();

  /**
   * Get roles participating on the Workflow.
   *
   * @return mixed
   */
  public function getRoles();

  /**
   * Get How to Change States option on the Workflow.
   *
   * @return string
   */
  public function getHowToChangeState();

  /**
   * Get the configuration about Show Workflow Comments.
   *
   * @return boolean
   */
  public function getShowComments();

  /**
   * Get all states for this workflow - ordered by weight.
   *
   * @return array
   */
  public function getStates();

  /**
   * Get all transitions for this workflow.
   *
   * @return array
   */
  public function getTransitions();

  /**
   * Get all user roles for this workflow.
   *
   * @return array
   */
  public function getUserRoles();

  /**
   * Set the rules that are participating in this workflow.
   *
   * @param $roles
   */
  public function setRoles($roles) ;
}
