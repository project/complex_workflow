<?php

/**
 * @file
 * WorkflowAccessCheck.php
 */

namespace Drupal\complex_workflow\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\complex_workflow\Entity\WorkflowHistory;


/**
 * Class WorkflowAccessCheck.
 *
 * Check access to Workflow.
 *
 * @package Drupal\complex_workflow\Access
 */
class WorkflowAccessCheck implements AccessInterface {

  private $entity;

  /**
   * WorkflowAccessCheck constructor.
   */
  public function __construct() {

    //$this->entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($id);
    $this->entity = \Drupal::entityTypeManager();
  }

  /**
   * Impact configuration access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return string
   *   A \Drupal\Core\Access\AccessInterface constant value.
   */
  public function access(AccountInterface $account) {
    if ($account->hasPermission('view workflow tab')) {
      return AccessResult::allowed()->cachePerPermissions();
    }
  }

  /**
   * Display Workflow tab only when entity has WorkflowState Field.
   *
   * @return AccessResult
   *   Allowed when has WorkflowState field, Forbidden if not and Neutral if
   *   it could not load entity data.
   */
  public function viewTab($node, $entity_id = 2, $entity_type = 'node') {

    $entity_id   = $node;
    $entity_type = 'node';
    $data        = $this->entity->getStorage($entity_type)->load($entity_id);

    if ($data) {

      // Check if entity has WorkflowField.
      $fields           = $data->getFieldDefinitions();
      $hasWorkflowField = FALSE;
      foreach ($fields as $field) {
        if ($field->getType() == 'workflow_field') {
          $hasWorkflowField = TRUE;
          break;
        }
      }

      if ($hasWorkflowField) {
        return AccessResult::allowed();
      }
      else {
        return AccessResult::forbidden();
      }
    }
    return AccessResult::neutral();
  }

  /**
   * Check if user has permission to edit transition comments.
   *
   * @param $node
   * @param $history_id
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function editComment($node, $history_id) {
    $current_user   = \Drupal::currentuser();

    if ($current_user->hasPermission('edit all workflow history comments')) {
      return AccessResult::allowed();
    }
    else {
      $history = $this->entity->getStorage('workflow_history')->load($history_id);
      $user_name = $history->getOwner()->getUsername();
      if ($user_name == $current_user->getAccountName() && $current_user->hasPermission('edit own workflow history comments')) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * Check if user has permission to revert transitions.
   *
   * @param $node
   * @param $history_id
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function revertTransition($node, $history_id) {
    $current_user = \Drupal::currentUser();

    if ($current_user->hasPermission('revert all workflow transitions')) {
      return AccessResult::allowed();
    }
    else {
      $history = $this->entity->getStorage('workflow_history')->load($history_id);
      $user_name = $history->getOwner()->getUsername();
      if ($user_name == $current_user->getAccountName() && $current_user->hasPermission('revert own workflow transitions') && $this->isLastTransition($node, $history_id)) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * Check if the transition is the last transition executed.
   *
   * @param $node
   * @param $history_id
   *
   * @return bool
   */
  private function isLastTransition($node, $history_id) {
    $node = $this->entity->getStorage('node')->load($node);
    $last = WorkflowHistory::getLastTransitionHistory($node);

    if ($last == $history_id) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}