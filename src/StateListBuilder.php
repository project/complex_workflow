<?php

namespace Drupal\complex_workflow;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a listing of State entities.
 */
class StateListBuilder extends DraggableListBuilder {

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type'   => 'item',
      '#markup' => $this->t("To create a new state, click on 'Add State' button. You may also drag it to the appropriate position.
      <br>A state must be marked as active, to be available in the workflow's transitions.
      <br>If you wish to inactivate a state that has content (i.e. count is not zero), then you need to select a state to which to reassign that content."),
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'complex_workflow_state_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label']    = $this->t('State');
    $header['id']       = $this->t('Machine name');
    $header['comments'] = $this->t('Comments');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id']    = [
      '#type'   => 'value',
      '#markup' => $entity->id(),
    ];

    $row['comments'] = [
      '#type'   => 'value',
      '#markup' => $entity->getComments(),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * @inheritdoc
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);

    foreach ($operations as $key => $value) {
      $operations[$key]['url']->setRouteParameters([
        'state'    => $entity->id(),
        'workflow' => $entity->getWorkflow(),
      ]);
    }

    return $operations;
  }

}
