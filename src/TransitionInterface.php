<?php

namespace Drupal\complex_workflow;

use Drupal\complex_workflow\Entity\State;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Session\UserSession;

/**
 * Provides an interface for defining Transition entities.
 */
interface TransitionInterface extends ConfigEntityInterface {

  /**
   * Is this Transition active?
   *
   * @return boolean
   */
  public function isActive();

  /**
   * Get the Transition comments.
   *
   * @return string
   */
  public function getComments();

  /**
   * Return the Transition initial State ID.
   *
   * @return string
   */
  public function getFrom();

  /**
   * Are comments required during this Transition?
   *
   * @return boolean
   */
  public function isCommentRequired();

  /**
   * Return the Transition final State ID.
   *
   * @return string
   */
  public function getTo();

  /**
   * Return the Warning Message configured to display during this Transition.
   *
   * @return string
   */
  public function getWarningMessage();

  /**
   * Return the parent Workflow ID for this Transition.
   *
   * @return string
   */
  public function getWorkflow();

  /**
   * Get all responsibilities for this transition.
   *
   * @return array
   */
  public function getResponsibilities();

  /**
   * Check if the user is allowed to execute this transition.
   *
   * @param \Drupal\Core\Session\UserSession $currentUser
   *
   * @return bool
   */
  public function isUserAllowed(UserSession $currentUser);
}
