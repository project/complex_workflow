<?php

namespace Drupal\complex_workflow\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;
use Drupal\complex_workflow\Util\WorkflowItems;

/**
 * Plugin implementation of the 'workflow_field' field type.
 *
 * @FieldType(
 *   id = "workflow_field",
 *   label = @Translation("Workflow State"),
 *   description = @Translation("Workflow"),
 *   category = @Translation("Workflow"),
 *   default_widget = "workflow_widget",
 *   default_formatter = "workflow_formatter"
 * )
 */
class WorkflowField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {

    return [
      'workflow_selected' => '',
      'max_length'        => 128,
      'case_sensitive'    => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Workflow State'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(TRUE);

    $properties['comments'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Transition Comments'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(FALSE);

    $properties['from_id'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Previous state'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(FALSE);

    $properties['uid'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('User Id'))
      ->setRequired(TRUE);

    $properties['timestamp'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Timestamp'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = [
      'columns' => [
        'from_id' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'uid' => [
          'type' => 'int',
        ],
        'timestamp' => [
          'type' => 'int',
        ],
        'value'    => [
          'type'     => 'varchar',
          'length'   => (int) $field_definition->getSetting('max_length'),
          'binary'   => $field_definition->getSetting('case_sensitive'),
          'not null' => TRUE,
        ],
        'comments' => [
          'type'     => 'varchar',
          'length'   => 4000,
          'not null' => FALSE,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {

    $constraints = parent::getConstraints();

    if ($max_length = $this->getSetting('max_length')) {
      $constraint_manager = \Drupal::typedDataManager()
        ->getValidationConstraintManager();
      $constraints[]      = $constraint_manager->create('ComplexData', [
        'value' => [
          'Length' => [
            'max'        => $max_length,
            'maxMessage' => t('%name: may not be longer than @max characters.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max'  => $max_length,
            ]),
          ],
        ],
      ]);
    }

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {

    $random          = new Random();
    $values['value'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {

    $elements = [];
    $items    = new WorkflowItems();

    $workflows      = $items->getWorkflows();
    $wf_options[''] = $this->t(' - Select -');
    foreach ($workflows as $workflow) {
      $wf_options[$workflow->get('id')] = $workflow->get('label');
    }

    if (!count($wf_options)) {
      drupal_set_message(
        $this->t('You must create at least one workflow before content can be
          assigned to a workflow.'), 'warning'
      );
    }

    $url                           = Url::fromRoute('entity.workflow.collection');
    $elements['workflow_selected'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Workflow'),
      '#options'       => $wf_options,
      '#default_value' => $this->getSetting('workflow_selected'),
      '#required'      => TRUE,
      '#description'   => $this->t('Choose the workflow for this entity. Manage 
        workflows <a href=":url">here</a>', [':url' => $url->toString()]),
      '#disabled'      => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {

    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * @inheritdoc
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element = [];
    return $element;
  }



}
