<?php

namespace Drupal\complex_workflow\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\complex_workflow\Util\WorkflowItems;

/**
 * Plugin implementation of the 'workflow_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "workflow_formatter",
 *   module = "complex_workflow",
 *   label = @Translation("Workflow State"),
 *   field_types = {
 *     "workflow_field"
 *   }
 * )
 */
class WorkflowFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $workflow_id = $items->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getSetting('workflow_selected');

    $wf_items = new WorkflowItems();
    $states   = $wf_items->getStates($workflow_id);

    foreach ($items as $delta => $item) {
      $label = $states[$this->viewValue($item)]->get('label');
      $elements[$delta] = ['#markup' => $label];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
