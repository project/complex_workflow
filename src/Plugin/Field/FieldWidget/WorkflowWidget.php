<?php

namespace Drupal\complex_workflow\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\Plugin\DataType\Timestamp;
use Drupal\complex_workflow\Util\WorkflowItems;

/**
 * Plugin implementation of the 'workflow_widget' widget.
 *
 * @FieldWidget(
 *   id = "workflow_widget",
 *   label = @Translation("Workflow State"),
 *   field_types = {
 *     "workflow_field"
 *   }
 * )
 */
class WorkflowWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {

    return parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = [];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $entity      = $items->getEntity();
    $field_name  = $this->fieldDefinition->getName();
    $wf_state    = $entity->get($field_name)->getValue();
    $workflow_id = $items->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getSetting('workflow_selected');
    $label       = $items->getDataDefinition()->getLabel();

    $wf_items = new WorkflowItems();
    $states   = $wf_items->getStates($workflow_id);

    if ($entity->isNew()) {

      $value = $workflow_id . '__' . 'entry_state';

      $element['#process'] = [[get_class($this), 'process']];

      $element['value'] = [
        '#type'  => 'value',
        '#value' => $value,
      ];

      $element['workflow_state'] = [
        '#type'   => 'markup',
        '#title'  => $label,
        '#markup' => $states[$value] ? $states[$value]->get('label') : $this->t('No state setted'),
      ];


    }
    elseif (isset($wf_state[0]['value'])) {
      $value = $wf_state[0]['value'];

      $element['workflow_state'] = [
        '#type'   => 'markup',
        '#title'  => $label,
        '#markup' => $states[$value] ? $states[$value]->get('label') : $this->t('No state setted'),
      ];
    }
    return $element;

  }

  /**
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $form
   *
   * @return array
   */
  public static function process($element, FormStateInterface $form_state, $form) {

    $element['uid'] = [
      '#type'  => 'value',
      '#value' => \Drupal::currentUser()->id(),
    ];

    $element['timestamp'] = [
      '#type'  => 'value',
      '#value' => time(),
    ];

    return $element;
  }

}
//@TODO set default value to workflow state.